export const BOOKS_LOADING = "BOOKS_LOADING";
export const BOOKS_FAILED = "BOOKS_FAILED";
export const ADD_BOOKS = "ADD_BOOKS";

export const ADD_CART = "ADD_CART";
export const GET_CART = "GET_CART";
export const CART_LOADING = "CART_LOADING";
export const CART_FAILED = "CART_FAILED";
export const REMOVE_CART = "REMOVE_CART";

export const CHECK_LOGIN = "CHECK_LOGIN";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const LOGIN_LOADING = "LOGIN_LOADING";
