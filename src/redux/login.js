import * as ActionTypes from "./ActionTypes";

export const Logins = (
  state = {
    isLoading: true,
    errMess: null,
    loginInfo: []
  },
  action
) => {
  switch (action.type) {
    case ActionTypes.CHECK_LOGIN:
      return {
        ...state,
        isLoading: false,
        errMess: null,
        loginInfo: action.payload
      };
    case ActionTypes.LOGIN_LOADING:
      return { ...state, isLoading: true, errMess: null, loginInfo: [] };

    case ActionTypes.LOGIN_FAILED:
      return {
        ...state,
        isLoading: false,
        errMess: action.payload,
        loginInfo: []
      };
    default:
      return state;
  }
};
