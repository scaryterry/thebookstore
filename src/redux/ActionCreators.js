import * as ActionTypes from "./ActionTypes";
import { baseUrl } from "../shared/baseUrl";

export const booksLoading = () => ({
  type: ActionTypes.BOOKS_LOADING
});

export const booksFailed = errmess => ({
  type: ActionTypes.BOOKS_FAILED,
  payload: errmess
});

export const cartsLoading = () => ({
  type: ActionTypes.CART_LOADING
});

export const cartsFailed = errmess => ({
  type: ActionTypes.CART_FAILED,
  payload: errmess
});

export const addBooks = books => ({
  type: ActionTypes.ADD_BOOKS,
  payload: books
});

export const checkLogin = loginData => ({
  type: ActionTypes.CHECK_LOGIN,
  payload: loginData
});

export const loginLoading = () => ({
  type: ActionTypes.LOGIN_LOADING
});

export const loginFailed = errmess => ({
  type: ActionTypes.LOGIN_FAILED,
  payload: errmess
});

export const doALogin = logindata => dispatch => {
  dispatch(loginLoading(true));
  return fetch(baseUrl + "login", {
    method: "POST",
    body: JSON.stringify(logindata),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
    .then(
      response => {
        if (response.ok) {
          localStorage.setItem("credentials", JSON.stringify(logindata));
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(response => {
      dispatch(checkLogin(response));
      dispatch(fetchCarts(response[0].id));
    })
    .catch(error => dispatch(loginFailed(error.message)));
};

export const doALogout = () => dispatch => {
  dispatch(loginLoading(true));
  return fetch(baseUrl + "login")
    .then(
      response => {
        if (response.ok) {
          localStorage.removeItem("credentials");
          return response;
        } else {
          var error = new Error(
            "Error to logout: " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(response => {
      dispatch(checkLogin(response));
      dispatch(fetchCarts());
    })
    .catch(error => dispatch(loginFailed(error.message)));
};

export const fetchBooks = () => dispatch => {
  dispatch(booksLoading(true));

  return fetch(baseUrl + "books")
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(books => dispatch(addBooks(books)))
    .catch(error => dispatch(booksFailed(error.message)));
};

export const addCart = cart => ({
  type: ActionTypes.ADD_CART,
  payload: cart
});

export const postToCart = book => dispatch => {
  const newpostToCart = {
    bookid: book.id,
    quantity: 1
  };

  return fetch(baseUrl + "users/" + book.userid + "/cart", {
    method: "POST",
    body: JSON.stringify(newpostToCart),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(response => {
      dispatch(addCart(response));
      alert("Successfully Added to Cart!\n");
    })
    .then(() => {
      dispatch(fetchCarts(book.userid));
    })
    .catch(error => {
      alert("Oops! Something went wrong.\n Please Try Again.");
    });
};

export const getCart = cart => ({
  type: ActionTypes.GET_CART,
  payload: cart
});

export const fetchCarts = id => dispatch => {
  return fetch(baseUrl + "users/" + id + "/cart")
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(cart => dispatch(getCart(cart)))
    .catch(error => dispatch(cartsFailed(error.message)));
};

export const delCart = cart => ({
  type: ActionTypes.REMOVE_CART,
  payload: cart
});

export const deleteCarts = book => dispatch => {
  return fetch(baseUrl + "users/" + book.userid + "/cart/" + book.id, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(response => {
      dispatch(delCart(response));
      alert("Successfully deleted from Cart!\n");
    })
    .then(() => {
      dispatch(fetchCarts(book.userid));
    })
    .catch(error => {
      alert("Oops! Something went wrong.\n Please Try Again.");
    });
};

export const deleteCartall = id => dispatch => {
  return fetch(baseUrl + "users/" + id + "/cart/", {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(response => {
      dispatch(delCart(response));
      alert("Checked out! You should receive the books in your email!\n");
    })
    .then(() => {
      dispatch(fetchCarts(id));
    })
    .catch(error => {
      alert("Oops! Something went wrong.\n Please Try Again.");
    });
};
