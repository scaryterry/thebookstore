import { createStore, combineReducers, applyMiddleware } from "redux";
import { Books } from "./books";
import { Carts } from "./carts";
import { Logins } from "./login";
import thunk from "redux-thunk";
import logger from "redux-logger";

export const ConfigureStore = () => {
  const store = createStore(
    combineReducers({
      books: Books,
      carts: Carts,
      logins: Logins
    }),
    applyMiddleware(thunk, logger)
  );
  return store;
};
