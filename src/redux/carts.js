import * as ActionTypes from "./ActionTypes";

export const Carts = (
  state = {
    isLoading: true,
    errMess: null,
    carts: []
  },
  action
) => {
  switch (action.type) {
    case ActionTypes.GET_CART:
      return {
        ...state,
        isLoading: false,
        errMess: null,
        carts: action.payload
      };
    case ActionTypes.CART_LOADING:
      return { ...state, isLoading: true, errMess: null, carts: [] };

    case ActionTypes.CART_FAILED:
      return { ...state, isLoading: false, errMess: action.payload, carts: [] };
    default:
      return state;
  }
};
