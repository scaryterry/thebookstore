import React, { Component } from "react";
import { baseUrl } from "../shared/baseUrl";
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavbarToggler,
  Collapse,
  NavItem,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  Button
} from "reactstrap";
import { NavLink } from "react-router-dom";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNavOpen: false,
      isModalOpen: false
    };
    this.toggleNav = this.toggleNav.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.logoutnow = this.logoutnow.bind(this);
  }

  toggleNav() {
    this.setState({
      isNavOpen: !this.state.isNavOpen
    });
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  logoutnow() {
    const { PerformLogout } = this.props;
    PerformLogout();
  }

  handleLogin(event) {
    this.toggleModal();
    var loginstring = [
      {
        username: this.username.value,
        password: this.password.value
      }
    ];
    const { PerformLogin } = this.props;
    PerformLogin(loginstring);

    event.preventDefault();
  }

  render() {
    return (
      <React.Fragment>
        <Navbar dark expand="md">
          <div className="container">
            <NavbarToggler onClick={this.toggleNav} />
            <NavbarBrand className="mr-auto" href="/">
              <img
                src={baseUrl + "images/logo.png"}
                height="30px"
                width="41"
                alt="The Book Store"
              />
            </NavbarBrand>
            <Collapse isOpen={this.state.isNavOpen} navbar>
              <Nav navbar>
                <NavItem>
                  <NavLink className="nav-link" to="/home">
                    <span className="fa fa-home fa-lg" /> Home
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="nav-link" to="/list">
                    <span className="fa fa-list fa-lg" /> List
                  </NavLink>
                </NavItem>
              </Nav>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <ShowButton
                    logins={this.props.logins}
                    toggle={this.toggleModal}
                    logoutnow={this.logoutnow}
                  />
                </NavItem>
                &nbsp;
                <NavItem>
                  <NavLink className="btn btn-outline-light btn-sm" to="/cart">
                    <span className="badge badge-secondary">
                      {this.props.counter}
                    </span>
                    &nbsp;
                    <span className="fa fa-shopping-bag" /> Checkout
                  </NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </div>
        </Navbar>
        <Modal
          isOpen={this.state.isModalOpen}
          toggle={this.toggleModal}
          logins={this.props.logins}
        >
          <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.handleLogin}>
              <FormGroup>
                <Label htmlFor="username">Username</Label>
                <Input
                  type="text"
                  id="username"
                  name="username"
                  innerRef={input => (this.username = input)}
                />
              </FormGroup>
              <FormGroup>
                <Label htmlFor="password">Password</Label>
                <Input
                  type="password"
                  id="password"
                  name="password"
                  innerRef={input => (this.password = input)}
                />
              </FormGroup>
              <Button type="submit" value="submit" className="bg-primary">
                Login
              </Button>
            </Form>
          </ModalBody>
        </Modal>
      </React.Fragment>
    );
  }
}

const ShowButton = props => {
  const { logins, toggle, logoutnow } = props;
  if (
    !logins.isLoading &&
    logins.errMess == null &&
    logins.loginInfo.length !== 0
  ) {
    return (
      <React.Fragment>
        <span style={{ color: "white" }}>
          {"Hi " + logins.loginInfo[0].username + "!"}
          &nbsp;
        </span>
        <button className="btn btn-outline-light btn-sm" onClick={logoutnow}>
          <span className="fa fa-sign-in" /> Logout
        </button>
      </React.Fragment>
    );
  } else {
    return (
      <button className="btn btn-outline-light btn-sm" onClick={toggle}>
        <span className="fa fa-sign-in" /> Login
      </button>
    );
  }
};

export default Header;
