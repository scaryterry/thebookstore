import React from "react";
import { baseUrl } from "../shared/baseUrl";
import StripeCheckout from "react-stripe-checkout";
import { key } from "../shared/stripeKey";

const ReactStripeCheckout = props => {
  const onToken = token => {
    /*fetch("/save-stripe-token", {
      method: "POST",
      body: JSON.stringify(token)
    }).then(response => {
      response.json().then(data => {
        alert(`We are in business, ${data.email}`);
      });
    });*/
    console.log("Button Clicked " + JSON.stringify(token));
    console.log(
      "Button Clicked " + JSON.stringify(props.logins.loginInfo[0].id)
    );
    props.CheckoutDeleteFromCart(props.logins.loginInfo[0].id);
  };

  return (
    <StripeCheckout
      token={onToken}
      stripeKey={key}
      amount={props.total}
      currency="USD"
      panelLabel="Pay"
      name="The Book Store"
      description="Complete your payment"
      image={baseUrl + "images/logo.png"}
    />
  );
};

export default ReactStripeCheckout;
