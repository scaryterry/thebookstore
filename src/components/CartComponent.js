import React, { Component } from "react";
import ReactStripeCheckout from "./CheckoutFormComponent";

import { Modal, ModalHeader, ModalBody } from "reactstrap";

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false
    };
    this.handleOnDelete = this.handleDelete.bind(this);
    this.handleOnAdd = this.handleAdd.bind(this);

    this.toggleModal = this.toggleModal.bind(this);
  }

  toggleModal() {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  handleAdd = event => {
    const { id } = event.target;
    const { loginInfo } = this.props.logins;
    alert(JSON.stringify(loginInfo[0].id));
    const AddToCart = this.props.AddToCart;
    let book = {
      id: parseInt(id, 10),
      userid: parseInt(loginInfo[0].id, 10)
    };
    AddToCart(book);
  };

  handleDelete = event => {
    const { id } = event.target;
    const { loginInfo } = this.props.logins;
    let book = {
      id: parseInt(id, 10),
      userid: parseInt(loginInfo[0].id, 10)
    };
    this.props.DeleteFromCart(book);
  };

  render() {
    const { isLoading, errMess, carts } = this.props.carts;
    const { books } = this.props.books;

    if (isLoading) {
      return (
        <div className="container">
          <div className="row">LOADING!!</div>
        </div>
      );
    } else if (errMess) {
      return (
        <div className="container">
          <div className="row">
            <h4>{errMess} </h4>
          </div>
        </div>
      );
    } else if (carts.status === 401) {
      return (
        <div className="container">
          <div className="row align-items-start">
            <div className="col-12 col-md m-1">
              <h4>Login to see items in your Cart.</h4>
            </div>
          </div>
        </div>
      );
    } else if (carts.length === 0) {
      return (
        <div className="container">
          <div className="row align-items-start">
            <div className="col-12 col-md m-1">
              <h4>Your Shopping Cart is empty! Start shopping now!</h4>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <React.Fragment>
          <Quantity
            books={books}
            carts={this.props.carts}
            handleOnDelete={this.handleOnDelete}
            handleOnAdd={this.handleOnAdd}
            logins={this.props.logins}
            CheckoutDeleteFromCart={this.props.CheckoutDeleteFromCart}
          />
          <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
            <ModalHeader toggle={this.toggleModal}>Checkout</ModalHeader>
            <ModalBody>
              <ReactStripeCheckout />
            </ModalBody>
          </Modal>
        </React.Fragment>
      );
    }
  }
}

export default Cart;

const DisplayCart = (books, carts) => {
  let booksname = [];
  let temparray = [];
  let cartids = [];
  let total = 0;
  if (carts.status !== 401) {
    books.forEach(function(book) {
      carts.forEach(function(cart) {
        if (book.id === cart.bookid) {
          let temp = {
            name: book.name,
            quantity: cart.quantity,
            price: book.price,
            id: cart.id,
            bookid: cart.bookid
          };
          total += book.price * cart.quantity;
          booksname.push(temp);
          cartids.push(cart.bookid);
        }
      });
    });
  }

  for (let i = 0; i <= booksname.length - 1; i++) {
    for (let j = i + 1; j <= booksname.length - 1; j++) {
      if (booksname[i].bookid === booksname[j].bookid) {
        booksname[i].quantity++;
      }
    }
    if (temparray.length !== 0) {
      let temparr = [];
      temparr = temparray.filter(temp => temp.bookid === booksname[i].bookid);
      if (temparr.length === 0) temparray.push(booksname[i]);
    } else {
      temparray.push(booksname[i]);
    }
  }
  booksname = temparray;
  return { booksname, total };
};

const Quantity = props => {
  const {
    books,
    carts,
    handleOnAdd,
    handleOnDelete,
    logins,
    CheckoutDeleteFromCart
  } = props;
  let { booksname, total } = DisplayCart(books, carts.carts);

  return (
    <div className="container">
      <div className="row">
        <h2>Cart Details</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Book Name</th>
              <th>Unit Price</th>
              <th>Quantity</th>
              <th>Total Price</th>
            </tr>
          </thead>
          <tbody>
            {booksname.map(book => {
              return (
                <tr key={book.id}>
                  <td>{book.name}</td>
                  <td>{book.price}</td>
                  <td>
                    <button
                      id={book.bookid}
                      className="btn btn-light btn-sm"
                      onClick={handleOnAdd}
                    >
                      +
                    </button>{" "}
                    {book.quantity}{" "}
                    <button
                      id={book.bookid}
                      className="btn btn-light btn-sm"
                      onClick={handleOnDelete}
                    >
                      -
                    </button>
                  </td>
                  <td>{book.price * book.quantity}</td>
                </tr>
              );
            })}
            <tr>
              <td />
              <td>
                <b>TOTAL</b>
              </td>
              <td>
                <b>{parseFloat(total).toFixed(2)}</b>
              </td>
              <td>
                <ReactStripeCheckout
                  total={parseFloat(total).toFixed(2) * 100}
                  logins={logins}
                  CheckoutDeleteFromCart={CheckoutDeleteFromCart}
                />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};
