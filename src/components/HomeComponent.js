import React from "react";
import { baseUrl } from "../shared/baseUrl";
import { Link } from "react-router-dom";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle
} from "reactstrap";

function RenderCard({ item, isLoading, errMess }) {
  if (isLoading) {
    return <h4>... Loading!</h4>;
  } else if (errMess) {
    return <h4>{errMess}</h4>;
  } else {
    return (
      <Link className="link" to={`/list/${item.id}`}>
        <Card>
          <CardImg src={baseUrl + item.image} alt={item.name} />
          <CardBody>
            <h3>{item.name}</h3>
            <CardTitle>
              <i>{item.mainevent}</i>
            </CardTitle>
            {item.designation ? (
              <CardSubtitle>{item.designation}</CardSubtitle>
            ) : null}
            <CardText>{item.description}</CardText>
          </CardBody>
        </Card>
      </Link>
    );
  }
}

function Home(props) {
  return (
    <div className="container">
      <div className="row align-items-start">
        <div className="col-12 col-md m-1">
          <RenderCard
            item={props.bstSlrBook}
            isLoading={props.bstSlrLoading}
            errMess={props.bstSlrErrMess}
          />
        </div>
        <div className="col-12 col-md m-1">
          <RenderCard
            item={props.bstRtdBook}
            isLoading={props.bstRtdLoading}
            errMess={props.bstRtdErrMess}
          />
        </div>
        <div className="col-12 col-md m-1">
          <RenderCard
            item={props.newLnchBook}
            isLoading={props.newLnchLoading}
            errMess={props.newLnchErrMess}
          />
        </div>
      </div>
    </div>
  );
}

export default Home;
