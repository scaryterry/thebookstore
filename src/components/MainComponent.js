import React, { Component } from "react";
import { Switch, Route, Redirect, withRouter } from "react-router-dom";
import {
  fetchBooks,
  postToCart,
  fetchCarts,
  deleteCarts,
  deleteCartall,
  doALogin,
  doALogout
} from "../redux/ActionCreators";
import { connect } from "react-redux";
import Header from "./HeaderComponent";
import Home from "./HomeComponent";
import List from "./ListComponent";
import Footer from "./FooterComponent";
import BookDetail from "./BookDetailComponent";
import Cart from "./CartComponent";

const mapStateToProps = state => {
  return {
    books: state.books,
    carts: state.carts,
    logins: state.logins
  };
};

const MapDispatchToProps = dispatch => ({
  fetchBooks: () => {
    dispatch(fetchBooks());
  },
  postToCart: (id, quantity) => {
    dispatch(postToCart(id, quantity));
  },

  fetchCarts: userid => {
    dispatch(fetchCarts(userid));
  },
  deleteCarts: book => {
    dispatch(deleteCarts(book));
  },
  doALogin: logindata => {
    dispatch(doALogin(logindata));
  },
  doALogout: () => {
    dispatch(doALogout());
  },
  deleteCartall: id => {
    dispatch(deleteCartall(id));
  }
});

class Main extends Component {
  componentDidMount() {
    if (localStorage.getItem("credentials") !== null) {
      this.props.doALogin(JSON.parse(localStorage.getItem("credentials")));
    }
    this.props.fetchBooks();
    this.props.fetchCarts(this.props.logins.loginInfo.id);
  }

  AddToCart = book => {
    this.props.postToCart(book);
  };

  DeleteFromCart = id => {
    this.props.deleteCarts(id);
  };

  CheckoutDeleteFromCart = id => {
    this.props.deleteCartall(id);
  };

  PerformLogin = logindata => {
    this.props.doALogin(logindata);
  };

  PerformLogout = () => {
    this.props.doALogout();
  };

  render() {
    const HomePage = () => {
      return (
        <Home
          bstSlrBook={this.props.books.books.filter(book => book.bestseller)[0]}
          bstSlrLoading={this.props.books.isLoading}
          bstSlrErrMess={this.props.books.errMess}
          bstRtdBook={this.props.books.books.filter(book => book.mostrated)[0]}
          bstRtdLoading={this.props.books.isLoading}
          bstRtdErrMess={this.props.books.errMess}
          newLnchBook={this.props.books.books.filter(book => book.newlaunch)[0]}
          newLnchLoading={this.props.books.isLoading}
          newLnchErrMess={this.props.books.errMess}
        />
      );
    };

    const BookWithId = ({ match }) => {
      const { books, isLoading, errMess } = this.props.books;
      return (
        <BookDetail
          book={
            books.filter(
              book => book.id === parseInt(match.params.bookId, 10)
            )[0]
          }
          isLoading={isLoading}
          ErrMess={errMess}
          AddToCart={this.AddToCart}
          logins={this.props.logins}
        />
      );
    };

    return (
      <div>
        <Header
          counter={this.props.carts.carts.length}
          PerformLogin={this.PerformLogin}
          PerformLogout={this.PerformLogout}
          logins={this.props.logins}
        />
        <div className="container">
          <div className="row">
            {" "}
            <Switch>
              <Route exact path="/home" component={HomePage} />
              <Route
                exact
                path="/list"
                component={() => <List books={this.props.books} />}
              />
              <Route path="/list/:bookId" component={BookWithId} />
              <Route
                exact
                path="/cart"
                component={() => (
                  <Cart
                    carts={this.props.carts}
                    books={this.props.books}
                    AddToCart={this.AddToCart}
                    DeleteFromCart={this.DeleteFromCart}
                    logins={this.props.logins}
                    CheckoutDeleteFromCart={this.CheckoutDeleteFromCart}
                  />
                )}
              />
              <Redirect to="/home" />
            </Switch>{" "}
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    MapDispatchToProps
  )(Main)
);
