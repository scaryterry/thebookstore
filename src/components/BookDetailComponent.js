import React, { Component } from "react";
import { baseUrl } from "../shared/baseUrl";
import { Card, CardImg, CardBody, CardTitle } from "reactstrap";

class BookDetail extends Component {
  render() {
    const { isLoading, errMess, book } = this.props;
    if (isLoading) {
      return (
        <div className="container">
          <div className="row">Loading...</div>
        </div>
      );
    } else if (errMess) {
      return (
        <div className="container">
          <div className="row">
            <h4>{errMess} </h4>
          </div>
        </div>
      );
    } else if (book != null) {
      return (
        <div className="container">
          <div className="row">
            <div className="col-12">
              <h3>{book.name}</h3>
              <hr />
            </div>
          </div>
          <div className="row">
            <RenderBook
              book={book}
              AddToCart={this.props.AddToCart}
              logins={this.props.logins}
            />
          </div>
        </div>
      );
    } else {
      return <div />;
    }
  }
}

const RenderBook = ({ book, AddToCart, logins }) => {
  const handleSubmit = () => {
    const { loginInfo } = logins;
    if (loginInfo.length !== 0) {
      let addbook = {
        id: parseInt(book.id, 10),
        userid: parseInt(loginInfo[0].id, 10)
      };
      AddToCart(addbook);
    } else {
      alert("You need to Login first");
    }
  };

  if (book != null) {
    return (
      <div className="row">
        <div className="col-12 col-md-5 mt-1">
          <Card>
            <CardImg width="100%" src={baseUrl + book.image} alt={book.name} />
            <CardBody>
              <CardTitle>{book.name}</CardTitle>
            </CardBody>
          </Card>
        </div>
        <div className="col-12 col-md-5 m-1 ">
          <p>{book.description}</p>
          <p>Book Category: {book.category}</p>
          <p>Price: ${book.price}</p>
          <button className="btn btn-info" onClick={handleSubmit}>
            Add to Cart
          </button>
        </div>
      </div>
    );
  } else {
    return <div />;
  }
};

export default BookDetail;
