import React from "react";
import { baseUrl } from "../shared/baseUrl";
import { Link } from "react-router-dom";

const List = props => {
  const { isLoading, errMess, books } = props.books;

  if (isLoading) {
    return (
      <div className="container">
        <div className="row">LOADING!!</div>
      </div>
    );
  } else if (errMess) {
    return (
      <div className="container">
        <div className="row">
          <h4>{errMess} </h4>
        </div>
      </div>
    );
  } else if (books != null) {
    return books.map(book => {
      return (
        <div key={book.id} className="col-12 col-md-3 mt-3">
          <RenderListItem book={book} />
        </div>
      );
    });
  }
};

function RenderListItem({ book }) {
  const { id, image, name, description } = book;
  return (
    <div>
      <Link className="link" to={`/list/${id}`}>
        <div className="card">
          <img className="card-img-top" src={baseUrl + image} alt={name} />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">{description}</p>
          </div>
        </div>
      </Link>
    </div>
  );
}

export default List;
