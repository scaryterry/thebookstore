1. Navigate to the folder containing the package.json file and run npm install to install all dependencies of the project.

2. Run npm start to run the project. Since the project is already coupled with browser sync, a browser will open with the page. Note that the react server will be running at port 3000.

3. Make sure the backend is running to get the application working properly.

Make sure both the console windows of react server and node are running when testing the application.
NOTE: CORS Chrome Extension might be needed for Cross Origin Reference issues. Install CORS and enable the same for proper working.
